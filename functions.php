<?php

function getSitesList($withHttpCode = false)
{
    include('sites.php');
    include('osIcones.php');

    ksort($sites);

    $lettreActuelle = null;
    $listeLettres = [];
    $all = [];
    $errors = [];

    foreach ($sites as $nom => $site) {
        if ($lettreActuelle !== $nom[0]) {
            $lettreActuelle = $nom[0];
            $listeLettres[] = $nom[0];
        }

        foreach ($site['liens'] as $os => $lien) {
            unset($httpCode);
            if ($withHttpCode and !is_null($lien)) {
                $header = get_headers($lien);
                preg_match("/\d{3}/", $header[0], $httpCode);
            }
            $httpCode = isset($httpCode[0]) ? $httpCode[0] : null;

            $all[$lettreActuelle][] = [
                'nom' => ucfirst($nom),
                'lien' => $lien,
                'icone' => $site['icone'],
                'osIcone' => $osIcones[$os],
                'editor' => $site['editor'],
                'nomEditor' => $site['nomEditor'],
                'comment' => $site['comment'],
                'colorClass' => $httpCode >= 300 ? "table-warning" : "table-success",
                'httpCode' => $httpCode,
            ];

            if ($httpCode >= 300) {
                $errors[$httpCode][] = [
                    'nom' => ucfirst($nom),
                    'lien' => $lien,
                    'icone' => $site['icone'],
                    'osIcone' => $osIcones[$os],
                ];
            }
        }
    }

    $resultats['all'] = $all;
    $resultats['errors'] = $errors;
    $resultats['listeLettres'] = $listeLettres;

    return $resultats;
}

function getHtmlLettres($listeLettres)
{
    $result = '';
    for ($ascii = 65; $ascii <= 90; $ascii++) {
        $lettre = chr($ascii);
        if (in_array(strtolower($lettre), $listeLettres)) {
            $result .= "<a class='btn btn-primary p-2 m-1' href='#lettre-$lettre'>$lettre</a>";
            continue;
        }
        $result .= "<a class='btn btn-secondary p-2 m-1'>$lettre</a>";
    }

    return $result;
}

function getHtmlAllSites($listSites)
{
    $result = '
    <table class="table">
        <thead>
            <tr>
                <th scope="col">
                    liens
                </th>
                <th scope="col">
                    Codes
                </th>
            </tr>
        </thead>
        <tbody>
    ';

    foreach ($listSites['all'] as $lettre => $resultat) {
        $lettre = ucfirst($lettre);

        $result .= "<tr class='table-secondary' id='lettre-$lettre' ><td colspan='2'>$lettre</td></tr>";

        foreach ($resultat as $site) {
            $nom = $site['nom'];
            $lien = $site['lien'];
            $icone = $site['icone'];
            $osIcone = $site['osIcone'];
            $colorClass = $site['colorClass'];
            $httpCode = $site['httpCode'];

            if (is_null($httpCode)) {
                continue;
            }

            $result .= "<tr>";
            $result .= "<td>";
            $result .= "<img class='icone' src='$icone'/>";
            $result .= "<img class='icone' src='$osIcone'/>";
            $result .= "$nom";
            $result .= "<br>";
            $result .= "<a target='_blank' href='$lien'>$lien</a>";
            $result .= "</td>";
            $result .= "<td class='$colorClass'>$httpCode</td>";
            $result .= "</tr>";
        }
    }

    $result .= '
        </tbody>
    </table>
    ';

    return $result;
}

function getHtmlPublicAllSites($listSites)
{
    $result = '
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">
                    Logiciels
                </th>
                <th scope="col">
                    Téléchargements
                </th>
            </tr>
        </thead>
        <tbody>
    ';

    foreach ($listSites['all'] as $lettre => $resultat) {
        $lettre = ucfirst($lettre);

        $result .= "<tr class='table-secondary' id='lettre-$lettre' ><td colspan='2'>$lettre</td></tr>";

        $logicielActuel = null;
        foreach ($resultat as $site) {
            $nom = $site['nom'];
            $lien = $site['lien'];
            $icone = $site['icone'];
            $editor = $site['editor'];
            $nomEditor = $site['nomEditor'];
            $comment = $site['comment'];
            $osIcone = $site['osIcone'];

            if ($nom != $logicielActuel) {
                if (!is_null($logicielActuel)) {
                    $result .= "</td>";
                    $result .= "</tr>";
                }
                $result .= "<tr>";
                $result .= "<td>";
                $result .= "<img class='icone' src='$icone'/>";
                $result .= $nom;
                $result .= "<br>";
                $result .= "<a target='_blank' href='$editor'>$nomEditor</a> ";
                $result .= "<br>";
                $result .= $comment;
                $result .= "</td>";
                $result .= "<td>";

                $logicielActuel = $nom;
            }
            if ($lien) {
                $result .= "<a target='_blank' href='$lien'><img class='icone' src='$osIcone'/></a>";
            } else {
                $result .= "<a target='_blank'><img class='icone no-lien' src='$osIcone'/></a>";
            }
        }
    }

    $result .= '
                </td>
            </tr>
        </tbody>
    </table>
    ';

    return $result;
}

function getHtmlErrorSites($listSites)
{
    $result = '
    <table class="table">
        <thead>
            <tr>
                <th>
                    Liens
                </th>
            </tr>
        </thead>
        <tbody>
    ';

    foreach ($listSites['errors'] as $code => $resultat) {
        $result .= "<tr class='table-secondary'><td scope='col'>$code</td></tr>";

        foreach ($resultat as $site) {
            $nom = $site['nom'];
            $lien = $site['lien'];
            $icone = $site['icone'];
            $osIcone = $site['osIcone'];

            $result .= "<tr>";
            $result .= "<td scope='row'>";
            $result .= "<img class='icone' src='$icone'/>";
            $result .= "<img class='icone' src='$osIcone'/>";
            $result .= "$nom";
            $result .= "<br>";
            $result .= "<a target='_blank' href='$lien'>$lien</a>";
            $result .= "</td>";
            $result .= "</tr>";
        }
    }

    $result .= '
        </tbody>
    </table>
    ';

    return $result;
}
