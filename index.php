<?php

include('functions.php');

$resultat = getSitesList(false);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test liens</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-grid.min.css" />
    <link href='https://css.gg/arrow-up-o.css' rel='stylesheet'>
    <link rel="stylesheet" href="style.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pub pub-horizontal">
                    Pub 1
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="header">
                    Logo
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 d-none d-sm-none d-md-block">
                <div class="pub">
                    Pub 2
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pub pub-horizontal">
                            Pub 3
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                            echo getHtmlLettres($resultat['listeLettres']);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            echo getHtmlPublicAllSites($resultat);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2 d-none d-sm-none d-md-block">
                <div class="pub">
                    Pub 4
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer">
                    Footer
                </div>
            </div>
        </div>
    </div>
    <div class="reup">
        <a href='#'>
            <i class="gg-arrow-up-o"></i>
        </a>
    </div>
</body>

</html>