<?php

$osIcones = [
    "windows" => "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Windows_logo_%E2%80%93_2012_%28dark_blue%29.svg/1024px-Windows_logo_%E2%80%93_2012_%28dark_blue%29.svg.png",
    "linux" => "https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg",
    "mac" => "https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Apple_Computer_Logo_rainbow.svg/1200px-Apple_Computer_Logo_rainbow.svg.png",
    "android" => "https://upload.wikimedia.org/wikipedia/commons/8/82/Android_logo_2019.svg",
    "chrome" => "https://p1.hiclipart.com/preview/484/278/340/simply-styled-icon-set-731-icons-free-google-chrome-alt-google-chrome-logo-png-clipart.jpg",
];
