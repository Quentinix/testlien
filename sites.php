<?php

$sites = [
    'cCleaner' => [
        'liens' => [
            'windows' => 'https://www.ccleaner.com/ccleaner/download',
            'mac' => 'https://www.ccleaner.com/ccleaner/download?mac',
            'linux' => null,
            'android' => 'https://play.google.com/store/apps/details?id=com.piriform.ccleaner',
            'chrome' => null,
        ],
        'icone' => 'https://s1.pir.fm/pf/icon/cc_128.png',
        'editor' => 'https://www.ccleaner.com/',
        'nomEditor' => 'Piriform',
        'comment' => 'Commentaire CCleaner',
    ],
    'teamViewer' => [
        'liens' => [
            'windows' => 'https://www.teamviewer.com/fr/telecharger/windows/',
            'mac' => 'https://www.teamviewer.com/fr/telecharger/mac-os/',
            'linux' => 'https://www.teamviewer.com/fr/telecharger/linux/',
            'android' => 'https://www.teamviewer.com/fr/telecharger/android/',
            'chrome' => null,
        ],
        'icone' => 'https://get.teamviewer.com/favicon.ico',
        'editor' => 'https://www.teamviewer.com/',
        'nomEditor' => 'TeamViewer',
        'comment' => 'Commentaire TeamViewer',
    ],
    'adobe Reader' => [
        'liens' => [
            'windows' => 'https://get.adobe.com/fr/reader/',
            'mac' => null,
            'linux' => null,
            'android' => null,
            'chrome' => null,
        ],
        'icone' => 'https://is5-ssl.mzstatic.com/image/thumb/Purple124/v4/06/ee/83/06ee8305-f875-41f5-dd2b-8160fa881a51/source/256x256bb.jpg',
        'editor' => 'https://www.adobe.com/',
        'nomEditor' => 'Adobe',
        'comment' => 'Commentaire Adobe Reader',
    ],
];
